---
bookCollapseSection: true
weight: 2
title: "Releases and Lifecycle Management"
---


# Managing the Application Life Cycle

<br><iframe width="560" height="315" src="https://www.youtube.com/embed/Lk493DLhl98" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<strong><h2>How to Add New App Releases</h2></strong>

Only enterprise applications can have multiple releases.

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the enterprise application from the Apps table of the publisher.</li>
    <li>Go to Add a new Release section of the application.</li>
    <li>Fill the form respectively. (Do not give the same apk for new release).</li>
    <li>Go to the previous release and change the state to <b>DEPRECATED</b>.</li>
    <li>Go to the newly added release and try to publish it.</li>
</ul>

<strong><h2>Managing the App Life Cycle</h2></strong>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Find the published application from the table.</li>
    <li>Go to the published application and change the state to <b>DEPRECATED</b> -> <b>RETIRED</b>.</li>
    <li>Then the app will be removed from the store and the app details will not show on the publisher.</li>
</ul>
