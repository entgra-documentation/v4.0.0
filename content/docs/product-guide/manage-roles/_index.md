---
bookCollapseSection: true
weight: 8
---

# Managing Roles

Entgra IoTS is shipped with a set of default roles. However, if required, tenant administrators are able to create new customized roles. Tenant administrators can use roles to manage the users and their devices, while end users allocated with device operation permissions can manage their own devices via the Entgra IoTS Console. Administrators can create roles, assign them to a user or a group of users, and edit or delete existing roles.

## Adding a Role and Assigning Permissions 


Follow the instructions below to add a role:

1.  [Sign in to the Entgra IoT Server console]({{< param doclink >}}product-guide/login-guide/).

    If you want to try out Entgra IoT Server as an administrator, use <strong>admin</strong> as the username and the password.

2.  You can navigate to the **ADD ROLE** page via the following methods: 

    1.  Method 01: 
        
        Click on the Menu.
    
        <img src = "../../image/4000.png" style="border:1px solid black "> 
    
        Select <strong>USER MANAGEMENT</strong>.
    
        <img src = "../../image/12345.png" style="border:1px solid black">
        
        Select <strong>ROLES</strong>.
        
        <img src = "../../image/352820159.png" style="border:1px solid black">
        
        Select <strong>ADD ROLE</strong>.  
        
        <img src = "../../image/1111.png" style="border:1px solid black">
        
    2.  Method 02: Click <strong>Add</strong> under <strong>ROLES</strong>. 
    
        <img src = "../../image/600058.png" style="border:1px solid black"> 

3.  Provide the required details and click <strong>Add Role</strong>.
    *   **Domain**: Provide the user store type from the list of items.
    *   **Role Name**: Provide the role name. 

    *   **User List**: Define the users belonging to the respective role. When you type the first few 
    characters of the username, the Entgra IoT Server will prompt a list of users having the 
    same characters. You can then select the users you wish to add.
    
    <img src = "../../image/352820286.png" style="border:1px solid black">

4.  Define the permissions that need to be associated with the role you created by selecting the permissions from the permission tree. 

    As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

    Make sure to select the **Login** permission. Without this permission, the users are unable to log in to Entgra IoT Server.

    <!--For more information on the APIs associated with the permissions, see [Permission APIs]({{< param doclink >}}using-entgra-iot-server/product-administration/user-management/#configuring-role-permissions).-->

### Assiging Role Permissions

<table border="1" width="100%">
   <colgroup>
      <col>
      <col>
   </colgroup>
   <tbody>
      <tr>
         <th bgcolor="#B8C4CC" width="40%"><font style="Arial" size="2">Permissions</th>
         <th bgcolor="#B8C4CC" width="60%"><font style="Arial" size="2">Description</th>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Applications Management</strong>
               <br>
               <img src = "../../image/Manage-App-Permissions.png">
         </td>
         <td><font style="Tahoma" size="2">
            <p>You can install applications on devices registered with Entgra IoT Server via the App Store or you can install applications via the internal REST APIs that is available on Entgra IoT Server. This permission ensures that a user is able to install and uninstall applications via the internal APIs that are available in Entgra IoT Server.</p>
            <p>For more information on installing applications via the App Store, see <a href="{{< param doclink >}}product-guide/app-management/install-and-uninstall-app/">Installing Mobile Apps</a>.</p>
         </td>
       <tr>
          <td><font style="Tahoma" size="2">
            <p><strong>Certificate Management</strong></p>
                 <img src = "../../image/RolePermissions_CertificateManagement.png">
          </td>
           <td><font style="Tahoma" size="2">
            <p>Entgra IoT Server supports mutual SSL, where the client verifies that the server can be trusted and the server verifies that the client can be trusted by using digital signatures. Following permissions grant access to client-side mutual SSL certificates:</p>
            <ul>
               <li><strong>device-mgt &gt; certificates &gt; manage</strong>: This permission enables to create certificates and access own certificates.</li>
               <li>
                  <strong>device-mgt &gt; admin &gt; certificates</strong>: These permissions ensure that a user is able to access all available certificates. Users with these permissions can:
                  <ul>
                     <li>View all certificates in a list view and in a detailed view</li>
                     <li>Create and remove certificates</li>
                     <li>Verify certificates: This allows an authorized user to authenticate and authorize a device by implementing on-behalf-of authentication.</li>
                  </ul>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
          <p><strong>Configurations Management</strong>
               <img src = "../../image/Configuration-Management.png">
         </td>
         <td><font style="Tahoma" size="2">
            <p>The monitoring frequency is configured under the general platform configurations in Entgra IoT Server. The IoT server uses this parameter to determine how often the devices enrolled with Entgra IoT Server need to be monitored.</p>
            <p>This permission enables users to configure, update and view the general platform configurations in Entgra IoT Server. In the general platform configurations, you need to define the monitoring frequent, which is how often the IoT server communicates with the device agent.</p>
            <p>For more information, see <a href="{{< param doclink >}}product-guide/device-management-guide/general-platform-configurations/">General Platform Configurations</a>.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Manage Devices</strong></p>
            <img src = "../../image/RolePermissions_DeviceManagement.png">
         </td>
         <td><font style="Tahoma" size="2">
            <ul>
               <li><strong>device-mgt &gt; any-device &gt; permitted-actions-under-owning-device</strong>: This permission enables you to view and manage all the devices shared with you.</li>
               <li>
                  <strong>device-mgt &gt; devices &gt; owning-device</strong>: These permissions enable users to:
                  <ul>
                     <li>Enroll and disenroll devices</li>
                     <li>Publish events received by the device client, to the analytics profile</li>
                     <li>Setup geofencing alerts</li>
                     <li>Modify device details such as name and description</li>
                     <li>Retrieve analytics for devices</li>
                  </ul>
               </li>
            </ul>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
         </td>
         <td><font style="Tahoma" size="2">This permission enables you to disenroll or unregister Android and Windows devices.</td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
         </td>
         <td><font style="Tahoma" size="2">This permission enables you to enroll or register Android, iOS and Windows devices with Entgra IoT Server.</td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Device Status</strong></p>
          <img src= "../../image/RolePermissions_ChangeDeviceStatus.png">  
         </td>
         <td><font style="Tahoma" size="2">This permission enables you to change a device status.</td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
         <p><strong>Device Operations</strong></p>
         <img src= "../../image/Device-Operations.png">  
         </td>
         <td><font style="Tahoma" size="2">Entgra IoT Server offers various device operations based on the mobile platform. This permission enables users to view and carry out device operations on their devices. Expand the preferred platform and select the operations that need to be enabled for users that belong to the role&nbsp;you are creating.</td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
          <p><strong>Platform Configurations</strong></p>
         <img src= "../../image/Platform-Configurations.png">  
         </td>
         <td><font style="Tahoma" size="2">
            <p>In Entgra IoT Server the settings can be customized for each platform. This permission enables you to maintain and customize the notification type, notification frequency, and the End User License Agreement (EULA) to suit the requirement of Android, iOS, and Windows mobile platform.</p>
            <p>For more information, see <a href="{{< param doclink >}}product-guide/enrollment-guide/enroll-android/android-configurations/" data-linked-resource-id="352822931" data-linked-resource-version="1" data-linked-resource-type="page">Android platform settings</a>, <a href="{{< param doclink >}}product-guide/enrollment-guide/enroll-ios/additional-server-configs-for-apple-devices/apple-server-configurations/#add-platform-configurations" data-linked-resource-id="352824279" data-linked-resource-version="1" data-linked-resource-type="page">iOS platform settings</a> and <a href="{{< param doclink >}}product-guide/enrollment-guide/enroll-windows/additional-server-configs-for-windows/" data-linked-resource-id="352824790" data-linked-resource-version="1" data-linked-resource-type="page">Windows platform settings</a>.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2"> 
         <p><strong>View Notifications</strong></p>
          <img src= "../../image/View-Notifications.png"> 
         </td>
         <td><font style="Tahoma" size="2">
            <p>The failure to carry out operations will be notified to the Entgra IoT Server administrator and the device owner. This permission enables you to view the notifications that were sent.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
         <p><strong>Manage Policies</strong></p>
         <img src= "../../image/Policy-Management.png"> 
         </td>
         <td><font style="Tahoma" size="2">
            <p>In Entgra IoT Server, you can define policies, which include a set of configurations. Entgra IoT Server policies are enforced on the Entgra IoT Server users' devices when new users register with the Entgra IoT Server. The Entgra IoT Server&nbsp;policy settings will vary based on the mobile OS type.</p>
            <p>This permission enables you to add, modify, view, publish, unpublish and remove policies.</p>
            <p>For more information on working with policies, see the relevant section (Android, iOS or Windows) under the <a target="_blank" href="../device-management-guide/"> Device Management Guide</a>.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
          <p><strong>Manage Roles</strong></p>
          <img src= "../../image/Role-Management.png"> 
         </td>
         <td><font style="Tahoma" size="2">
            <p>Entgra IoT Server allows you to create new customized roles.&nbsp;This permission enables you to add, modify, view and remove roles.</p>
            <p>For more information on working with roles, see <a href="{{< param doclink >}}product-guide/manage-roles/" data-linked-resource-id="352820118" data-linked-resource-version="1" data-linked-resource-type="page">Managing Roles</a>.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
          <p><strong>Manage Users</strong></p>
          <img src= "../../image/User-Management.png"> 
         </td>
         <td><font style="Tahoma" size="2">
            <p>Entgra IoT Server allows you to create and manage users. This permission enables you to add, modify, view and remove users.</p>
            <p>For more information on working with users, see <a href="{{< param doclink >}}/product-guide/manage-users/" data-linked-resource-id="352820590" data-linked-resource-version="1" data-linked-resource-type="page">Managing Users</a>.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Manage Groups</strong></p>
             <img src= "../../image/RolePermissions_GroupManagement.png"> 
          </td>
         <td><font style="Tahoma" size="2">
            <p>These permissions enable you to manage groups pertaining to devices and user roles. The user role related permission enables viewing all user roles available in Entgra IoT Server. The device related permissions enable you to:</p>
            <ul>
               <li>Create and remove device groups</li>
               <li>Assign devices to a group</li>
               <li>Remove devices from a group</li>
               <li>View the list of groups attached to a device</li>
               <li>View the list of roles that have access to a group</li>
               <li>View the groups accessible by the logged in user</li>
            </ul>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
         <p><strong>Mobile Application Management</strong></p>
         <img src= "../../image/Mobile-App-Mgt.png"> 
         </td>
         <td><font style="Tahoma" size="2">
            <p>You are able to create mobile apps in the App Publisher that is available in Entgra IoT Server. In order to create, publish, delete, install and update mobile applications the required permissions must be selected.</p>
            <p>To enable users to subscribe to applications and install an application on a device via the App Store you need to select Subscribe that is under the Web App permissions.</p>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Device Type Management</strong></p>
            <img src= "../../image/RolePermissions_DeviceTypeManagement.png"> 
            </td>
         <td><font style="Tahoma" size="2">
            <p>Following permissions enable managing device types:</p>
            <ul>
               <li><strong>device-mgt &gt; device-type &gt; add</strong>: This enables the ability to add or delete event definitions for device types.</li>
               <li><strong>device-mgt &gt; devicetype &gt; deploy</strong>: This enables deploying device type components via API. It is recommended to grant this permission to device admin users.</li>
            </ul>
         </td>
      </tr>
      <tr>
         <td><font style="Tahoma" size="2">
            <p><strong>Authorization Management</strong></p>
            <img src= "../../image/RolePermissions_AuthorizationManagement.png"> 
          </td>
         <td><font style="Tahoma" size="2">Users with this permission can check whether a user has the permission to access and manage a device. It is recommended to grant this permission to device admin users.</td>
      </tr>
   </tbody>
</table>

5.  Click <strong>Update Role Permission</strong>.


## Configuring Role Permissions

This section provides details on how to configure permissions by defining permissions to an API and the permissions associated with the APIs.

### Defining Permissions for APIs

If you wish to create additional permission, follow the steps given below:

1.  Navigate to the JAX-RS web application that of your device types API folder. For more information, see the [permission XML file of the virtual fire-alarm.](https://github.com/wso2/carbon-device-mgt-plugins/blob/v4.0.55/components/device-types/virtual-fire-alarm-plugin/org.wso2.carbon.device.mgt.iot.virtualfirealarm.api/src/main/webapp/META-INF/permissions.xml)
2.  Define the new permission using the `@permission` annotation.  
    The `scope` defines to whom the API is limited to and the `permission` that is associated with a given API.  
    Example:

    `@Permission(scope = "virtual_firealarm_user", permissions = {"/permission/admin/device-mgt/user/operations"})`

3.  Restart Entgra IoT Server and you will see the new permission created in the permission tree.  
    Now only users who have this specific permission assigned to them will be able to control the buzzer of the fire-alarm.

### Permission APIs

Let's take a look at the default permissions associated with the APIs.

Permissions related to the Entgra IoTS Administrator (admin)

<table border="1" width="100%">
   <tbody>
      <tr>
         <th bgcolor="#B8C4CC" width="30%"><font style="Arial" size="2">Permissions</th>
         <th bgcolor="#B8C4CC" width="70%"><font style="Arial" size="2">Description</th>
      </tr>
      <tr>
      <td><font style="Arial" size="2">device-mgt/admin/dashboard</td>
       <td><font style="Arial" size="2">Permission to access the WSO2 IoT Server analytics dashboard.</td>
      </tr>
       <tr>
       <td><font style="Arial" size="2">device-mgt/admin/devices</td>
        <td><font style="Arial" size="2">Permission to access the APIs related to devices.</td>
        </tr>
         <tr>
         <td><font style="Arial" size="2">device-mgt/admin/devices/list</td>
          <td><font style="Arial" size="2">Permission to access the get all devices API.</td>
          </tr>
           <tr>
          <td><font style="Arial" size="2">device-mgt/admin/devices/view</td>
           <td><font style="Arial" size="2">Permission to access and retrieve device information from the APIs.</td>
           </tr>
            <tr>
             <td><font style="Arial" size="2">device-mgt/admin/groups</td>
              <td><font style="Arial" size="2">	Permission to access the APIs related to groups.</td>
              </tr>
        <tr>
        <td><font style="Arial" size="2">device-mgt/admin/device-mgt/admin/groups/list</td>
        <td><font style="Arial" size="2">Permission to access the get all groups API.</td>
        </tr>
        <tr>
        <td><font style="Arial" size="2">device-mgt/admin/groups/roles</td>
        <td><font style="Arial" size="2">Permission to access the API that gets all the roles added to a group.</td>
        </tr>
        <tr>
        <td><font style="Arial" size="2">device-mgt/admin/groups/roles/permission</td>
        <td><font style="Arial" size="2">Permission to access the API that gets all the permissions associates with the roles that can access groups.</td>
        </tr>
        <tr>
        <td><font style="Arial" size="2">device-mgt/admin/groups/roles/add</td>
        <td><font style="Arial" size="2">Permission to access the API that enable a role to be added to a group.</td>
        </tr>
        <tr>
        <td><font style="Arial" size="2">device-mgt/admin/groups/roles/delete</td>
        <td><font style="Arial" size="2">Permission to access the API that enable a role to be deleted from a group.</td>
        </tr>
         <tr>
         <td><font style="Arial" size="2">device-mgt/admin/information/get</td>
         <td><font style="Arial" size="2">Permission to access the get all information API.</td>
         </tr>
          <tr>
          <td><font style="Arial" size="2">device-mgt/admin/notifications</td>
           <td><font style="Arial" size="2">Permission to access the APIs related to notifications.</td>
           </tr>
</tbody>
</table>

## Default Roles and Permissions


By default, Entgra IoTS includes a set of roles. These default roles and permissions have been explained in the following subsections.

### Default User Roles

The following roles are available by default in Entgra IoTS:

<ul style="list-style-type:lower-roman;">
<li><strong>admin</strong></li>
<li><strong>internal-devicemgt-user</strong></li>
<li><strong>internal-appmgt-user</strong></li>
</ul>

*   i. <strong>admin</strong>- Role assigned to the super tenant administrator by default.

    If you are defining the permissions for an IoTS administrator who needs to perform operations and configure policies, make sure to select **admin**. The **admin** permission allows the user to perform operations and configure policies for devices.

     If you wish to create a user with administrative permission other than the default administrator in Entgra IoTS, follow the steps given below:

    1.  [Add a new a role](#adding-a-role-and-permissions).
    2.  Configure role permissions by specifically selecting the **admin** permission.

*   ii. <strong>internal-devicemgt-user</strong> - This is a system reserved role with the minimum set of permissions to carry out operations. When a user creates an account before accessing the device management console the user is assigned the internal-device-mgt role by default.

    iii. <strong>internal-appmgt-user</strong> - This role has the minimum set of permissions to carry out application management on the device. 

### Permissions Associated with User Roles

<table border="1">
  <tbody>
    <tr>
      <th bgcolor="#B8C4CC"><font style="Arial" size="2">User Role</th>
      <th bgcolor="#B8C4CC"><font style="Arial" size="2">Allows Actions</th>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">admin</td>
      <td><font style="Tahoma" size="2">The super tenant administrator belongs to this role. By default, a super tenant administrator will have full control on all the device management consoles.</td>
    </tr>
    <tr>
      <td><font style="Tahoma" size="2">devicemgt-user</td>
      <td><font style="Tahoma" size="2">
        <p>Carryout external operations on a device based on the permissions assigned via the permission tree.</p>
        <p>Example: getting device details, registering a device control the buzzer and many more.</p>
      </td>
    </tr>
    <tr>
          <td><font style="Tahoma" size="2">app-mgt-user</td>
          <td><font style="Tahoma" size="2">
            <p>Carryout application management operations via the store and publisher, based on the permissions assigned via the permission tree.</p>
            <p>Example: Managing application lifecycle and subscriptions, installing and uninstalling apps etc.</p>
          </td>
        </tr>
  </tbody>
</table>

## Removing a Role


Follow the instructions below to update a role:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}product-guide/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:1px solid black ">
    
2.  Click <strong>User Management</strong>.

    <img src = "../../image/20202020.png" style="border:1px solid black ">
    
3.  Click <strong>Role</strong>.  

    <img src = "../../image/30303030.png" style="border:1px solid black ">
    
4.  Click <strong>Remove</strong> on the role you wish to remove. 
 
    <img src = "../../image/40404040.png" style="border:1px solid black ">
    
    Click <strong>REMOVE</strong> to confirm that you want to remove the role. 
     
    <img src = "../../image/352820457.png" style="border:1px solid black ">



## Searching, Filtering and Sorting Roles


### Searching for Users

Follow the instructions given below to search for roles:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}product-guide/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:1px solid black ">
    
2.  Click <strong>User Management</strong>.

    <img src = "../../image/20202020.png" style="border:1px solid black ">
    
3.  Click <strong>Role</strong>.  

    <img src = "../../image/30303030.png" style="border:1px solid black ">
    
4.  Search for roles using the search bar.  

    <img src = "../../image/50505050.png" style="border:1px solid black ">

### Filtering Users

Follow the instructions below to filter roles:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}product-guide/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:1px solid black ">
    
2.  Click <strong>User Management</strong>.

    <img src = "../../image/20202020.png" style="border:1px solid black ">
    
3.  Click <strong>Role</strong>.  

    <img src = "../../image/30303030.png" style="border:1px solid black ">
    
4.  Filter the roles by the role name.  

    <img src = "../../image/50505050.png" style="border:1px solid black ">

## Updating a Role 


Follow the instructions below to update a role:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}product-guide/login-guide/)Sign in 
to the IoTS device management console) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:1px solid black ">
    
2.  Click <strong>User Management</strong>.

    <img src = "../../image/20202020.png" style="border:1px solid black ">
    
3.  Click <strong>Role</strong>.  

    <img src = "../../image/30303030.png" style="border:1px solid black ">
    
4.  Click <strong>Edit</strong> on the role you wish to update.  

    <img src = "../../image/23232323.png" style="border:1px solid black ">
    
5.  Update the required filed and click Update Role.  

    *   <strong>Domain</strong>: Provide the user store type from the list of items.
    *   <strong>Role Name</strong>: Provide the role name.

        <img src = "../../image/10101010.png" style="border:1px solid black ">



## Updating Role Permissions


Follow the instructions below to configure the role permissions:

1.  [Sign in to the IoTS device management console]({{< param doclink >}}product-guide/login-guide/) and click the menu icon.

    <img src = "../../image/352820436.png" style="border:1px solid black ">
    
2.  Click <strong>User Management</strong>.

    <img src = "../../image/20202020.png" style="border:1px solid black ">
    
3.  Click <strong>Role</strong>.  

    <img src = "../../image/30303030.png" style="border:1px solid black ">
    
4.  Click <strong>Edit Permissions</strong> on the role you wish to configure.  

    <img src = "../../image/222222.png" style="border:1px solid black ">

5.  Select or remove the permissions as required. The levels of authority for granting permissions are illustrated in the table below.  
   
    As the permissions are categorized, when the main permission category is selected, all its sub-permissions will get selected automatically. 

 ### Authority Levels for Granting Permission
 
<table border="1" width="100%">
  <tbody>
    <tr>
      <th width="40%" bgcolor="#B8C4CC"><font style="Arial" size="2">Authority Level</th>
      <th width="60%" bgcolor="#B8C4CC"><font style="Arial" size="2">Permission Level</th>
    </tr>
        <tr>
          <td><img src = "../../image/Update_Role_Permissions.JPG" style="border:1px solid black ">
          </td>
          <td><font style="Tahoma" size="2"> Make sure to select the <strong>Log-in</strong> permission. Without this permission, the users are unable to log in to Entgra IoT Server.
             </td>
        </tr>
        <tr>
           <td> <img src = "../../image/permission_tree01.JPG" style="border:1px solid black">
           </td>
           <td><font style="Tahoma" size="2">First Level expansion of authority levels.</td>
         </tr>
         <tr>
         <td> <img src = "../../image/device_mgmt_component_set01.JPG" style="border:1px solid black">
         </td>
         <td valign="top"><font style="Tahoma" size="2">First level of device management permission levels with <strong>enterprise</strong>, <strong>roles</strong>, <strong>authorization</strong>, <strong>topics</strong> and <strong>device-type</strong> expanded and randomly selected.
          <br><br>
          <ul style="list-style-type:disc;">
                 <li><strong>enterprise:</strong>
                  <ul style="list-style-type:circle;">
                  <li><strong>user</strong> - if selected, the user will be able to modify and view as well.</li>
                  <li><strong>modify</strong> - user is able to modify access to enterprise.</li>
                  <li><strong>view</strong> - user can only view who has access to enterprise.</li>
                  </ul></li></ul>
          <ul style="list-style-type:disc;">
           <li><strong>roles:</strong>
           <ul style="list-style-type:circle;">
            <li><strong>roles</strong> -  when selected, the user is able to view and manage role permissions.</li>
            <li><strong>view</strong> - user is able to modify access to enterprise.</li>
            <li><strong>manage</strong> - user can only view who has access to enterprise.</li>
            </ul></li></ul>
            <ul style="list-style-type:disc;">
            <li><strong>authorization:</strong>
             <ul style="list-style-type:circle;">
             <li><strong>authorization</strong> - when selected, the user is able to authorize and verify the enrolled device.</li>
             <li><strong>verify</strong> - user can verify the device.</li>
             </ul></li></ul>
             <ul style="list-style-type:disc;">
              <li><strong>topics:</strong>
              <ul style="list-style-type:circle;">
              <li><strong>topics</strong> - user can modify and view the topics.</li>
              <li><strong>view</strong> - user is only able to view the topics.</li>
              </ul></li></ul>
              <ul style="list-style-type:disc;">
              <li><strong>device-type:</strong>
              <ul style="list-style-type:circle;">
              <li><strong>features</strong> 
              <ul style="list-style-type:square;">
              <li><strong>features</strong> - the user is able to view and change features.</li>
              <li><strong>view</strong> - user can view the features.</li>
              </li></ul>
              <li><strong>config</strong> - user can verify the device.</li>
              <ul style="list-style-type:square;">
              <li><strong>config</strong> - user can change the device configuration.</li>
               <li><strong>view</strong> - user can only view the configurations.</li>
               </ul>
               <li><strong>view</strong> - user can view the device type.</li>
               <li><strong>add</strong> - user can add new device types.</li>                                             
               </ul></li></ul>
               </td>
         </tr>
         <tr>
                  <td> <img src = "../../image/device_mgmt_component_set02.JPG" style="border:1px solid black">
                  </td>
           <td valign="top"><font style="Tahoma" size="2">First level of device management permission levels with <strong>notifcations</strong> and <strong>devices</strong> expanded and selected.
                    <br><br>
                    <ul style="list-style-type:disc;">
                           <li><strong>notifications:</strong>
                            <ul style="list-style-type:circle;">
                            <li><strong>notifications</strong> - the user is able to activate notifications.</li>
                            <li><strong>view</strong> - user can only view notifications.</li>
                           </ul></li></ul>
                    <ul style="list-style-type:disc;">
                     <li><strong>devices:</strong>
                     <ul style="list-style-type:circle;">
                      <li><strong>dep</strong> - the device is enrolled with DEP and is able to perform functions allowed by DEP.</li>
                      <li><strong>add</strong> - </li>
                      <li><strong>view</strong> - </li>
                      <li><strong>disenroll</strong> - </li>
                      <li><strong>enroll</strong> - </li>                                      
                      <li><strong>any-device</strong> - </li>                                      
                      <li><strong>tenants</strong> - </li>                                      
                      <li><strong>change-status</strong> - </li>                                      
                      <li><strong>owning-device</strong> - </li>                                      
                      </ul></li></ul>
                     </td>        
         </tr>
                    <tr>
                    <td> <img src = "../../image/device_mgmt_component_set04.JPG" style="border:1px solid black">
                    </td>
                    <td>
                    <font style="Tahoma" size="2">First level of device management permission levels with <strong>reporting</strong> and <strong>admin</strong> expanded and randomly selected. 
                    <ul style="list-style-type:disc;">
                    <li><strong>reporting</strong>
                    <ul style="list-style-type:circle;">
                    <li>analytics-admin</li>
                    <ul style="list-style-type:square;">
                    <li><strong>query</strong></li>
                    <li><strong>view</strong></li>
                    <li><strong>delete</strong></li>
                    <li><strong>add</strong></li>
                    </ul>
                    <li>analytics</li>
                     <ul style="list-style-type:square;">
                     <li><strong>bluetooth-beacon</strong></li>
                     <li><strong>view</strong></li>
                    </ul></ul>
                    <ul style="list-style-type:disc;">
                    <li><strong>admin</strong>
                    <ul style="list-style-type:circle;">
                    <li>device-type</li>
                    <ul style="list-style-type:square;">
                    <li><strong>device-type</strong></li>
                    <li><strong>view</strong></li>
                    <li><strong>config</strong></li>
                    </ul>
                   <li>certificates</li>
                     <ul style="list-style-type:square;">
                     <li><strong>certificates</strong></li>
                     <li><strong>verify</strong></li>
                     <li><strong>details</strong></li>
                     <li><strong>delete</strong></li>
                     <li><strong>view</strong></li>
                     <li><strong>add</strong></li>
                     </ul>
                     <li>groups</li>
                     <ul style="list-style-type:square;">
                     <li><strong>view</strong></li>
                     <li><strong>add</strong></li>
                     </ul>
                     <li>devices</li>
                     <ul style="list-style-type:square;">
                     <li><strong>permenant-delete</strong></li>
                     <li><strong>update-enrollment</strong></li>
                     </ul>
                    </font></td></tr>    
                    <tr>
                    <td> <img src = "../../image/device_mgmt_component_set05.JPG" style="border:1px solid black">
                    </td>
                    <td>
                    <font style="Tahoma" size="2">First level of device management permission levels with <strong>device</strong>, <strong>platform-configurations</strong> and <strong>metadata</strong> expanded and randomly selected. 
                    <ul style="list-style-type:disc;">
                    <li><strong>device</strong>
                    <ul style="list-style-type:circle;">
                    <li>application</li>
                    <li>review</li>
                    <li>subscription</li>
                    <li>admin</li>
                    </ul>
                    </li>
                    <li><strong>owning-device</strong></li>
                    </ul>
                    </font></td></tr>  
                    <tr>
                    <td> <img src = "../../image/device_mgmt_component_set06.JPG" style="border:1px solid black">
                     </td>
                     <td>
                     <font style="Tahoma" size="2">First level of device management permission levels with <strong>analytics</strong>, <strong>devicetype</strong> and <strong>policies</strong> expanded and randomly selected. 
                     <ul style="list-style-type:disc;">
                     <li><strong>tenants</strong>
                     <ul style="list-style-type:circle;">
                     <li>application</li>
                     <li>review</li>
                     <li>subscription</li>
                     <li>admin</li>
                     </ul>
                     </li>
                     <li><strong>owning-device</strong></li>
                     </ul>
                     </font></td></tr>   
                     <tr>
                     <td> <img src = "../../image/device_mgmt_component_set07.JPG" style="border:1px solid black">
                     </td>
                     <td>
                     <font style="Tahoma" size="2">First level of device management permission levels with <strong>certificates</strong>, <strong>groups</strong>, <strong>applications</strong> and <strong>users</strong> expanded and randomly selected. 
                                        <ul style="list-style-type:disc;">
                                        <li><strong>tenants</strong>
                                        <ul style="list-style-type:circle;">
                                        <li>application</li>
                                        <li>review</li>
                                        <li>subscription</li>
                                        <li>admin</li>
                                        </ul>
                                        </li>
                                        <li><strong>owning-device</strong></li>
                                        </ul>
                                        </font></td></tr>      
         <tr>
         <td> <img src = "../../image/app_mgmt_permission_levels01.JPG" style="border:1px solid black">
         </td>
         <td>
         <font style="Tahoma" size="2">Application management permission levels expanded and randomly selected. 
         First level:
        <ul style="list-style-type:disc;">
         <li><strong>Store</strong>
         <ul style="list-style-type:circle;">
         <li>application</li>
         <ul style="list-style-type:square;">
         <li><strong>application</strong> - user is able to view and update all the in-house corporate applications through the Store.</li>
          <li><strong>review</strong> - user is able to review the application.</li>
          <li><strong>subscription</strong> - user can view, update subscriptions to the application.</li>
          <li><strong>admin</strong> - user has administration rights to the application via the Store.</li>
            </ul>
         <li>review</li>
          <ul style="list-style-type:square;">
                  <li><strong>review</strong> - user can review access to the Store.</li>
                   <li><strong>update</strong> - user is able to able to update the application via the Store.</li>
                   <li><strong>view</strong> - user is able to view the application.</li>
                                      </li>
                   </ul>
         <li>subscription</li>
          <ul style="list-style-type:square;">
          <li><strong>subscription</strong> - user can access/update subscritions to applications in the Store.</li>
          <li><strong>install</strong> - user is able to install selected apps via the Store.</li>
          <li><strong>uninstall</strong> - user has rights uninstall selected apps via the Store.</li>
           </li>
            </ul>
         <li>admin</li>
         <ul style="list-style-type:square;">
                   <li>subscription</li>
                   <ul style="list-style-type:lower-roman;">
                            <li><strong>subscription</strong> - user can update subsriptions to the applications.</li>
                            <li><strong>view</strong> - user is able to view subscriptions to the applications.</li>
                            </ul>  
                   <li>review</li>
                   <ul style="list-style-type:lower-roman;">
                    <li><strong>review</strong> - user is able to review application administration access.</li>
                    <li><strong>update</strong> - usercan update access to application administration.</li>
                    </ul>  
                   </li>
         </ul>
         </li>
         <li><strong>Publisher</strong>
         <ul style="list-style-type:circle;">
         <li><strong>admin:</strong></li>
         <ul style="list-style-type:square;">
         <li><strong>review</strong>
         <ul style="list-style-type:lower-roman;">
         <li><strong>review</strong> - user is able to view and review the application in the Publisher portal.</li>
         <li><strong>view</strong> - user is able to view the application.</li>
         </ul>         
         </li>
         <li><strong>application</strong></li>
         <ul style="list-style-type:lower-roman;">
                  <li><strong>application</strong> - user is able to access and update the application.</li>
                  <li><strong>update</strong> - user can update the application.</li>
                  </ul>         
          </ul>
         <li><strong>application:</strong>
         <ul style="list-style-type:square;">
         <li><strong>application</strong> - user is able to view and update the application via the Publisher portal.    
         </li>
         <li><strong>view</strong> - user can only view the application.</li>
         <li><strong>update</strong> - user is able to update the application.</li>
          </ul>
         </li>
         </ul>
         </li>
         </ul>
         </td>
          </tr>
          </tbody>
    </table>
     
5.  Select the appropriate permission levels and click <strong>Update Role Permissions</strong>.

    <img src = "../../image/352820567.png" style="border:1px solid black ">
