---
bookCollapseSection: true
weight: 2
---

# Installing the Entgra Android Device Management App

Android devices are enrolled and managed using an application that is installed on the device, known as the Device Management Agent App. The Entgra Agent app can be installed by downloading it directly either from the Entgra IoT server, or via the Playstore, as described below. 

{{< expand "Download from Entgra IoT server" "..." >}}

There is an agent app bundled with the IoT server and that can be downloaded to your mobile device and installed.

<!-- <strong>Prerequisites</strong>
{{< expand "The server has to be downloaded and started." "..." >}}
<iframe src="{{< param doclink >}}product-guide/enroll-android/install-agent/" onload="this.width=screen.width;this.height=screen.height;">html-name</iframe>
{{< /expand >}} -->

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li>Server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started.</a></li>
    <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal.</a></li>
</ul>
{{< /hint >}}

### Step 1  
Click <strong>+ Add</strong> under the <strong>Device</strong> tab.

<img src = "../../../../image/2000.png" style="border:5px solid black ">

### Step 2
Click  <strong>Android</strong> under device types.

<img src = "../../../../image/2001.png" style="border:5px solid black ">

### Step 3
Click  <strong>Get Android Agent</strong> under device types.

<img src = "../../../../image/2002.png" style="border:5px solid black ">

### Step 4
Scan the QR code using the mobile device. New Android OS version includes the QR code scanning app in-built. If this is not available, a QR code scanning app can be downloaded from the Playstore. Follow the QR code scanned link to download the agent to the device.

Alternatively, this page can be assed via the mobile device browser's itself (follow steps 1-3 on the phone's broswer) and click on <strong>Download APK</strong> to download the APK to the device.

### Step 5

Once the app is downloaded, click <strong>Open</strong>. Depending on the OS version of the device, unknown sources has to be enabled to install the agent. In Android 8 and upwards, once you click Open, the following screen will be displayed. Select <strong>Allow from this source</strong> and go back.

<img src = "../../../../image/2004.jpg" style="border:5px solid black ">

### Step 6

Click <strong>Install</strong> to install the agent.

<img src = "../../../../image/2005.jpg" style="border:5px solid black ">

{{< /expand >}}
{{< expand "Download from playstore" "..." >}}

## Download from the Playstore

{{< hint info >}}
<strong>Prerequisit</strong>
<ul style="list-style-type:disc;">
    <li>Server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started</a>.</li>
    <li>Logged into the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal</a>.</li>
</ul>
{{< /hint >}}

Visit the link https://play.google.com/store/apps/details?id=io.entgra.iot.agent using your mobile device or go to Google Playstore app in the mobile device and search for <strong>Entgra Device Management Agent</strong> and install the app.
{{< /expand >}}