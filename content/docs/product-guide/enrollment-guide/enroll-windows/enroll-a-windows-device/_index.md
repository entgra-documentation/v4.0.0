---
bookCollapseSection: true
weight: 2
---

# Enroll a Windows Device

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
    <li>Server has to be <a href="{{< param doclink >}}guide-to-work-with-the-product/download-and-start-the-server/">downloaded and started</a>.</li>
    <li>Must have been logged on to the server's <a href="{{< param doclink >}}guide-to-work-with-the-product/login-guide/">Device Management Portal</a>.</li>
    <li>Click <strong>Add device (https://{IP}:{port}/devicemgt/device/enroll)</strong>.</li>
    <li>Click on <strong>Windows</strong> from <strong>DEVICE TYPES</strong>.</li>
</ul>
{{< /hint >}}

<iframe width="560" height="315" src="https://www.youtube.com/embed/6BYVQ5wvEpc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<h2>Steps</h2>

<ul style="list-style-type:decimal;">
    <li>Add host configuration to the windows host files.</li>
    <li>Add the server certificate to <strong>trusted root certification authorities</strong>.</li>
    <li>Go to <strong>access to work or school</strong> in Windows settings.</li>
    <li>Click <strong>enroll only in device management</strong>.</li>
    <li>Input admin@
        <server address> in the pop-up window.</li>

</ul>