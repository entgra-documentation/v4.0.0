---
bookCollapseSection: true
weight: 4
---

# iOS Device Enrollment (BYOD) with Agent

{{< hint info >}}
<strong>Prerequisites</strong>
<ul style="list-style-type:disc;">
   <li>Server has to be <a href="{{< param doclink >}}product-guide/download-and-start-the-server/">downloaded and started</a>.</li>
   <li>Must have been logged on to the server's <a href="{{< param doclink >}}product-guide/login-guide/">Device Management Portal</a>.</li>
   <li>Click <strong>Add Device</strong> (https://{IP}:{port}/devicemgt/device/enroll)</li>
    <li>Click <strong>iOS</strong> under <strong>DEVICE TYPES</strong></li>
    <li>Scan the QR code that appears with a QR code scanning app or type <strong>https://IP:port/ios-web-agent/enrollment</strong> in safari browser.</li>
</ul>
{{< /hint >}}

## If the Device is above iOS 12.2 
(If you have installed OS updates after March 2019)
 

<iframe width="560" height="315" src="https://www.youtube.com/embed/yyR62HLQtew" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>


## If the Device is below iOS 12.2 

<iframe width="560" height="315" src="https://www.youtube.com/embed/WHwSU9p5d90" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen>
</iframe>