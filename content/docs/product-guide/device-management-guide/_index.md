---
bookCollapseSection: true
weight: 5
title: "Device Management Guide"
---

# Guide to Managing Devices

In managing the devices enrolled with the Entgra IoT server, it is important to set the general platform configurations to the frequency that the server needs to be monitored. Instructions for general platform configurations are given <a href="general-platform-configurations/">here</a>.

Managing devices section guides you on how to group enrolled devices, share the groups specific to user roles, and to update and remove groups when required. This section also takes you through on how to monitor the device status.

Also covered within this section are the Policies for Android, Apple and Windows devices. Procedures on How to Add, Update, Publish/Unpublish and View a policy enforced on a device are given. It is also possible to manage the priority order of the policies on a device.

Device operations that can be executed on Android, Apple and Windows devices via Entgra IoT server have been explained separately for each device type. Similarly, device policies that can be enforced on each device type are shown under Android Device Policies, Apple Device Policies and Windows Device Policies.

Device Management documentation available here guides you completely on how to effectively manage your device with the Entgra IoT server.

Given below are the procedures and detailed explanations in managing your devices and of operations and policies applicable to each:
<div>
<ul style="list-style-type:disc;">
<li><a href="general-platform-configurations">General Platform Configurations</a></li>
<li><a href="manage-enrolled-devices">Manage Enrolled Devices</a></li>
<li><a href="android-devices">Android Devices</a></li>
<li><a href="apple-devices">Apple Devices</a></li>
<li><a href="windows-devices">Windows Devices</a></li>
</ul>
</div>
