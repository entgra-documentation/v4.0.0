---
bookCollapseSection: true
weight: 15
---

# App Usage Time Configuration 

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an Android device.
{{< /hint >}}

<strong>Screen Usage Configuration</strong>

This policy is for monitor the apps screen usage. 
Once this policy is applied by giving app package names, allowed time and the period time the policy will monitor whether user use the given apps violently.


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow notification for screen usage</strong></td>
            <td>By enabling notification, user will receivee a notification of violated applications names.</td>
        </tr>
        <tr>
            <td colspan = "2"><strong><center>Application List & Configuration List</center></strong></td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.gm ]</td>
        </tr>
        <tr>
            <td><strong>Allow Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Allow Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Period Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Period Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Allow To Block</strong></td>
            <td>By enabling this, user can allow to block the specific applications after exceed the
                usage.This will work only when the device has the device owner.</td>
        </tr>                                             
    </tbody>
</table>

<strong>Network Usage Configuration</strong>

This policy is for monitor the apps network usage.
Once this policy is applied by giving app package names, allowed data,type,network type and the period time the policy will monitor whether user use the given apps violently.
Once the app exceed the allowed data limit ,the VPN will apply automatically.

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow notification for screen usage</strong></td>
            <td>By enabling notification, user will receivee a notification of violated applications names.</td>
        </tr>
        <tr>
            <td colspan = "2"><strong><center>Application List & Configuration List</center></strong></td>
        </tr>
        <tr>
            <td><strong>Package Name</strong></td>
            <td>Eg: [ com.google.android.gm ]</td>
        </tr>
        <tr>
            <td><strong>Allow Data</strong></td>
            <td>Eg: 10,25</td>
        </tr>
        <tr>
            <td><strong>Allow Data Type</strong></td>
            <td>Drop down values Eg: MB,GB</td>
        </tr>
        <tr>
            <td><strong>Network Type</strong></td>
            <td>Drop down values Eg: Wifi,Mobile,...</td>
        </tr>        
        <tr>
            <td><strong>Period Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Period Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Allow To Block</strong></td>
            <td>By enabling this, user can allow to block the specific applications after exceed the
            usage.This will work only when the device has the device owner.</td>
        </tr>                                             
    </tbody>
</table>

<strong>Global Screen Usage Configuration</strong>

This policy is for monitor the all installed apps screen usage. 
Once this policy is applied by giving  allowed time and the period time the policy will monitor whether user use the installed apps violently.
By enabling the allow block feature, all installed applications will black list after exceed the allowed time value


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Allow Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Period Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Period Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Allow To Block</strong></td>
            <td>This will work only when the device has the device owner.</td>
        </tr>                                             
    </tbody>
</table>

<strong>Global Network Usage Configuration</strong>

This policy is for monitor the all apps network usage. 
Once this policy is applied by giving specific data, the policy will monitor whether user use the apps violently.
By enabling the allow VPN feature, VPN will apply to all applications after exceed the allowed data limit


<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Allow Data</strong></td>
            <td>Eg: 10,25</td>
        </tr>
        <tr>
            <td><strong>Allow Data Type</strong></td>
            <td>Drop down values Eg: MB,GB</td>
        </tr>
        <tr>
            <td><strong>Network Type</strong></td>
            <td>Drop down values Eg: Wifi,Mobile,...</td>
        </tr>        
        <tr>
            <td><strong>Period Time Type</strong></td>
            <td>Drop down values Eg: Minute,Hour,...</td>
        </tr>
        <tr>
            <td><strong>Period Time</strong></td>
            <td>Eg: [ time ]</td>
        </tr>
        <tr>
            <td><strong>Allow to apply VPN</strong></td>
            <td>If the agent isn't add as a trusted VPN,this feature won't work.</td>
        </tr>                                             
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/android-device/android-device
-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}