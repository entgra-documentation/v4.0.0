---
bookCollapseSection: true
weight: 2
---

# Restrictions

<b>Camera</b> - Check if the camera is working or not based on the policy

<b>Disable Location</b> - Add a get location command and the location will not complete

<b>Disable storage</b> - SD cards and USB drives are not allowed. When plugging in one, an error will be displayed

<img src="../../../../../image/windows_policy_4.png" style="border:5px solid black">

<b>Disable one drive - </b> Reboot the device and the syncing of files will be diasabled in one 
drive

<b>Disable Bluetooth.</b>

<b>Disable cellular data.</b>

<b>Disable data roaming.</b>

<b>Disable connected devices.</b>

<b>Disable connect with PC - </b>
Policy requires device restart.
    
    Goto -> settings -> phone

The following screen will be displayed with the link phone button greyed out

<img src="../../../../../image/windows_policy_5.png" style="border:5px solid black">

<b>Disable VPN configuration.</b>

<b>Disable VPN roaming.</b>

<b>Disable date time - </b>
Policy requires device restart.

<b>Disable the ability to change date time settings.</b>
    
    Goto Settings -> Time & Language -> Date & time

Following screen will be displayed with configurations grayed out

<img src="../../../../../image/windows_policy_6.png" style="border:5px solid black">


<b>Disable Non microsoft accounts.</b>

The policy requires the device to restart to apply.
Prevents adding non-microsoft accounts to the device. To verify, google

    Go to settings -> Accounts -> Emails & accounts -> Click Add an account 
    
Following will be shown

<img src="../../../../../image/windows_policy_7.png" style="border:5px solid black">

<b>Disable private windows in browser - </b>
The policy requires the device to restart to apply.
Blocks user from creating private windows in Microsoft Edge browser

<img src="../../../../../image/windows_policy_8.png" style="border:5px solid black">

<b>Disable indexing of removable drives - </b>
This will stop the searchability of files in a usb drive via explorer.

<b>Disable language settings - </b>
The policy requires the device to restart to apply.
User will not be able to change the prefered language of the device.
    
       Go to settings -> Time & Language -> Language 
Windows display language and preferred languages sections will be greyed out. 

<img src="../../../../../image/windows_policy_9.png" style="border:5px solid black">

<b>Disable region settings - </b>
The policy requires the device to restart to apply.
User will not be able to change the region of the device.
    
    Go to settings -> Time & Language -> Region 
Region related settings will be greyed out. 

<img src="../../../../../image/windows_policy_10.png" style="border:5px solid black">


<b>Disable cortana - </b>
User will be blocked from using Cortana

<img src="../../../../../image/windows_policy_11.png" style="border:5px solid black">
