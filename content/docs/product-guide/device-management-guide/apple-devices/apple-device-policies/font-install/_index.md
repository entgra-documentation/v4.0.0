---
bookCollapseSection: true
weight: 16
---

# Font Install

{{< hint info >}}
<b> <a href ="{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#add-a-policy"> Add policy </a></b>in Policy description page gives a brief description how a policy is added to an iOS device.
{{< /hint >}}

This configurations can be used to add an additional font to an iOS device.

<i>Please note that * sign represents required fields of data.</i>

<table style="width: 100%;">
    <colgroup>
        <col>
            <col>
    </colgroup>
    <tbody>
        <tr>
            <th><strong>Data keys of Policy</strong></th>
            <th>Description</th>
        </tr>
        <tr>
            <td><strong>Font name</strong></td>
            <td>The user-visible name for the font. This field is replaced by the actual name of the font after installation.
            </td>
        </tr>
        <tr>
            <td><strong>Font file</strong></td>
            <td>The contents of the font file.<br>
            <i>Each payload must contain exactly one font file in TrueType (.ttf) or OpenType (
            .otf) format. Collection formats (.ttc or
            .otc) are not supported.</i>
            </td>
        </tr>
    </tbody>
</table>

{{< hint info >}}
<b> <a href = "{{< param doclink >}}guide-to-work-with-the-product/device-management-guide/apple-device/apple-device-policies/#publish-a-policy"> Publish a policy </a></b>in Policy description page gives a brief description how a policy is published.
 {{< /hint >}}
