---
bookCollapseSection: true
weight: 3
title: "Clustering IoT Server"
---

# Clustering the IoT Serever

## Entgra IoT Deployment Pattern
The following diagram illustrates a typical deployment pattern for the Entgra IoT Server.
<br><br>
<img src = "../../image/IOT_01.png" style="border:2px solid black ">
<center>IoT Server Deployment</center>
<br>

As indicated in the above diagram, when clustering the IoT Server, there is worker manager separation. In a standard WSO2 product cluster, worker and manager separation is derived from deployment synchronization. However, this differs from standard WSO2 Carbon worker manager separation.

Entgra IoT Server includes an admin console that can be used by any user with administrative privileges. These users can perform operations on enrolled devices and the devices can retrieve those actions by requesting for the pending operations. This is done by either walking the device through a push notification or configuring the device to poll at a pre-configured frequency.

Normally administrative tasks should be run from a manager node.

There are two major deployment patterns for the manager node. One could be running the manager node in the private network due to security constraints and other is allowing end users to access the management node so that they can control and view their devices.

A manager node is used to run background tasks that are necessary to update device information such as location and the list of installed applications. For more information on creating different profiles in the IoT Server, see <a href="https://docs.wso2.com/display/IoTS310/Product+Profiles" target="_blank">Product Profiles</a>.

Let's take a look at the steps to cluster the IoT Server:

<ul style="list-style-type:disc;">
<li><a href="https://docs.wso2.com/display/IoTS310/Configuring+the+Load+Balancer" target="_blank"> Configuring the Load Balancer</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Setting+Up+the+Databases+for+Clustering" target="_blank"> Setting Up the Databases for Clustering</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Mounting+the+Registry" target="_blank"> Mounting the Registry</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Configuring+the+Key+Manager+Node" target="_blank"> Configuring the Key Manager Node</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Configuring+the+Manager+Node" target="_blank"> Configuring the Manager Node</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Configuring+the+Worker+Nodes" target="_blank"> Configuring the Worker Nodes</a></li>
<li><a href="https://docs.wso2.com/display/IoTS310/Clustering+the+iOS+Server" target="_blank"> Clustering the iOS Server</a></li>
</ul>

**Before you begin, you need the following to cluster the IoT Server:**

<table border="1" bgcolor="#D9DEE1">
<tr><td><font style="Arial" size="2">
<strong>Virtual machines used in a high availability cluster</strong>
The following is a list of virtual machines (VMs) that are used in a high availability cluster and their details.
<ul style="list-style-type:disc;">
<li>Manager - 1 VM</li>
<li>Worker - 2 VMs</li>
<li>Key manager - 2 VMs</li>
<li>DBs - 1 MySQL instance</li>
</ul>
All the VMs have 4 cores and 4GB memory.

<strong>Open ports</strong>

80 and 443 are from the NGINX server.
<br><br>
The following ports need to be opened for Android and iOS devices so that it can connect to GCM (Google Cloud Message) and APNS (Apple Push Notification Service) and enroll to WSO2 IoT Server.

<strong>Android</strong>

The ports to open are 5228, 5229 and 5230. GCM typically uses only 5228, but it sometimes uses 5229 and 5230.

GCM does not provide specific IPs, so it is recommended to allow the firewall to accept outgoing connections to all IP addresses contained in the IP blocks listed in Google's ASN of 15169.  

<strong>iOS</strong>
<ul style="list-style-type:disc;">
<li>5223 - TCP port used by devices to communicate to APNS servers</li>
<li>2195 - TCP port used to send notifications to APNS</li>
<li>2196 - TCP port used by the APNS feedback service</li>
<li>443 - TCP port used as a fallback on Wi-Fi, only when devices are unable to communicate to APNS on port 5223</li>
</ul>
<br>
The APNS servers use load balancing. The devices will not always connect to the same public IP address for notifications. The entire 17.0.0.0/8 address block is assigned to Apple, so it is best to allow this range in the firewall settings.
</td></tr>
</table>


