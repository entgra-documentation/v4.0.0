---
bookCollapseSection: true
weight: 4
title: "Configuring the Key Manager Node"
---

# Configuring the Key Manager Node

Through out this guide you have configured keymgt.iots310.wso2.com as the key manager node.

<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
Before you begin:
<ul style="list-style-type:disc;">
<li>Mount the registry as explained <a href="../registry/">here</a>.</li>
<li>Configure the following databases for the Key Manager in the &lt;IOTS_HOME&gt;/conf/datasources/master-datasources.xml file.</li>
For more information, see <a href="../databases/">Setting Up the Databases for Clustering</a>.</li>
<ul style="list-style-type:circle;">
<li>Registry Database</li>
<li>User manager database</li>
<li>APIM Database</li>
</ul></ul>
</font></td></tr></table>

Let's start configuring the Key Manager node.
<ol>
<li>Configure the HostName and MgtHostName properties in the <I>&lt;IOTS_HOME&gt;/conf/carbon.xml</I> file as shown below.</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;HostName&gt;keymgt.iots310.wso2.com&lt;/HostName&gt;<br>
&lt;MgtHostName&gt;keymgt.iots310.wso2.com&lt;/MgtHostName&gt;
</font></td></tr></table>
<li>Configure the <I>&lt;IOTS_HOME&gt;/bin/iot-server.sh</I> file as shown below:</li>
<strong>Note:</strong> Make sure to have the Offset property configured to zero. If it is set to a value other than zero, you need to update the NGINX configuration based on the port offset.
<br><br>
<li>Configure all the following properties in the <I>&lt;IOTS_HOME&gt;/conf/identity/sso-idp-config.xml</I> file by replacing <I>https://localhost:9443</I> with <I>https://mgt.iots310.wso2.com:443</I>.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<ul style="list-style-type:disc;">
<li>AssertionConsumerServiceURL</li>
<li>DefaultAssertionConsumerServiceURL</li>
Refer to <strong>Note 2</strong> below to view a configured file.
</ul>
</font></td></tr></table>
 </li>
<li>Start the WSO2 IoT Server's core profile.</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
cd &lt;IOTS_HOME&gt;/bin<br>
 iot-server.sh
</font></td></tr></table>
</ol>

<strong>Note 2:</strong>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;SSOIdentityProviderConfig&gt;
   &lt;TenantRegistrationPage&gt;https://stratos-local.wso2.com/carbon/tenant-register/select_domain.jsp&lt;/TenantRegistrationPage&gt;
   &lt;ServiceProviders&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;devicemgt&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/devicemgt/uuf/sso/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/devicemgt/uuf/sso/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignAssertion&gt;true&lt;/SignAssertion&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;EnableAttributeProfile&gt;false&lt;/EnableAttributeProfile&gt;
         &lt;IncludeAttributeByDefault&gt;false&lt;/IncludeAttributeByDefault&gt;
         &lt;Claims&gt;
            &lt;Claim&gt;http://wso2.org/claims/role&lt;/Claim&gt;
            &lt;Claim&gt;http://wso2.org/claims/emailaddress&lt;/Claim&gt;
         &lt;/Claims&gt;
         &lt;EnableAudienceRestriction&gt;true&lt;/EnableAudienceRestriction&gt;
         &lt;EnableRecipients&gt;true&lt;/EnableRecipients&gt;
         &lt;AudiencesList&gt;
            &lt;Audience&gt;https://localhost:9443/oauth2/token&lt;/Audience&gt;
         &lt;/AudiencesList&gt;
         &lt;RecipientList&gt;
            &lt;Recipient&gt;https://localhost:9443/oauth2/token&lt;/Recipient&gt;
         &lt;/RecipientList&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;store&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/store/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/store/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;CustomLoginPage&gt;/store/login.jag&lt;/CustomLoginPage&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;social&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/social/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/social/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;CustomLoginPage&gt;/social/login&lt;/CustomLoginPage&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;publisher&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/publisher/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/publisher/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;CustomLoginPage&gt;/publisher/controllers/login.jag&lt;/CustomLoginPage&gt;
         &lt;EnableAudienceRestriction&gt;true&lt;/EnableAudienceRestriction&gt;
         &lt;AudiencesList&gt;
            &lt;Audience&gt;carbonServer&lt;/Audience&gt;
         &lt;/AudiencesList&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;API_STORE&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/api-store/jagg/jaggery_acs.jag&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/api-store/jagg/jaggery_acs.jag&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;EnableAudienceRestriction&gt;true&lt;/EnableAudienceRestriction&gt;
         &lt;AudiencesList&gt;
            &lt;Audience&gt;carbonServer&lt;/Audience&gt;
         &lt;/AudiencesList&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;portal&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/portal/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/portal/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;EnableAudienceRestriction&gt;true&lt;/EnableAudienceRestriction&gt;
         &lt;EnableRecipients&gt;true&lt;/EnableRecipients&gt;
         &lt;AudiencesList&gt;
            &lt;Audience&gt;https://localhost:9443/oauth2/token&lt;/Audience&gt;
         &lt;/AudiencesList&gt;
         &lt;RecipientList&gt;
            &lt;Recipient&gt;https://localhost:9443/oauth2/token&lt;/Recipient&gt;
         &lt;/RecipientList&gt;
      &lt;/ServiceProvider&gt;
      &lt;ServiceProvider&gt;
         &lt;Issuer&gt;analyticsportal&lt;/Issuer&gt;
         &lt;AssertionConsumerServiceURLs&gt;
            &lt;AssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/portal/acs&lt;/AssertionConsumerServiceURL&gt;
         &lt;/AssertionConsumerServiceURLs&gt;
         &lt;DefaultAssertionConsumerServiceURL&gt;https://mgt.iots310.wso2.com:443/portal/acs&lt;/DefaultAssertionConsumerServiceURL&gt;
         &lt;SignResponse&gt;true&lt;/SignResponse&gt;
         &lt;EnableAudienceRestriction&gt;true&lt;/EnableAudienceRestriction&gt;
         &lt;EnableRecipients&gt;true&lt;/EnableRecipients&gt;
         &lt;AudiencesList&gt;
            &lt;Audience&gt;https://localhost:9443/oauth2/token&lt;/Audience&gt;
         &lt;/AudiencesList&gt;
         &lt;RecipientList&gt;
            &lt;Recipient&gt;https://localhost:9443/oauth2/token&lt;/Recipient&gt;
         &lt;/RecipientList&gt;
      &lt;/ServiceProvider&gt;
   &lt;/ServiceProviders&gt;
&lt;/SSOIdentityProviderConfig&gt;
</code>
</pre>

</font></td></tr></table>


