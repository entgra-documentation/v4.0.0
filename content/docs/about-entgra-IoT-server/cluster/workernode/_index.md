---
bookCollapseSection: true
weight: 6
title: "Configuring the Worker Nodes"
---

# Configuring the Worker Nodes

Throughout this guide, you have configured _iots310.wso2.com_ and _gateway.iots310.wso2.com_ as the worker node.

<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
Before you begin:
<ul style="list-style-type:disc;">
<li>Mount the registry as explained <a href="../registry/">here</a>.</li>
<li>Configure the following databases for the Key Manager in the <I> &lt;IOTS_HOME&gt;/conf/datasources/master-datasources.xml </I> file.</li>
For more information, see <a href="../databases/">Setting Up the Databases for Clustering</a>.</li>
<ul style="list-style-type:circle;">
<li>Registry Database</li>
<li>User manager database</li>
<li>APIM Database</li>
<li>App manager database and include the social and storage database schemas to the same database.</li>
<li>CDM database and include the certificate management, android, iOS and windows database schemas to the same database.</li>
</ul></ul>
</font></td></tr></table>
Let's start configuring the worker nodes.
<ol>
<li>Configure the <I>HostName</I> and  <I>MgtHostName</I> properties in the <I>&lt;IOTS_HOME&gt;/conf/carbon.xml</I> file as shown below.</li>
<table width="400 px" border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
<pre>
<code>
&lt;HostName&gt;iots310.wso2.com&lt;/HostName&gt;
&lt;MgtHostNam&gt;mgt.iots310.wso2.com&lt;/MgtHostNam&gt;
</font></td></tr></table>
<strong>Note:</strong><br>
Make sure to have the Offset property configured to zero. If it is set to a value other than zero, you need to update the NGINX configuration based on the port offset.
<br><br>
<li>Configure the <I>&lt;IOTS_HOME&gt;/bin/iot-server.sh</I> file as shown below:</li>
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
-Diot.manager.host="mgt.iots310.wso2.com" \<br>
-Diot.manager.https.port="443" \<br>
-Diot.core.host="iots310.wso2.com" \<br>
-Diot.core.https.port="443" \<br>
-Diot.keymanager.host="keymgt.iots310.wso2.com" \<br>
-Diot.keymanager.https.port="443" \<br>
-Diot.gateway.host="gateway.iots310.wso2.com" \<br>
-Diot.gateway.https.port="443" \<br>
-Diot.gateway.http.port="80" \<br>
-Diot.gateway.carbon.https.port="443" \<br>
-Diot.gateway.carbon.http.port="80" \<br>
-Diot.apimpublisher.host="mgt.iots310.wso2.com" \<br>
-Diot.apimpublisher.https.port="443" \<br>
-Diot.apimstore.host="mgt.iots310.wso2.com" \<br>
-Diot.apimstore.https.port="443" \<br>
</pre></code>
</font></tr></td></tr></table>

<li>Disable task monitoring in the <I>android.xml</I> and <I>windows.xml</I> files that are in the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/devicetypes</I> directory as shown below:
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;TaskConfiguration&gt;<br>
    &lt;Enable&gt;false&lt;/Enable&gt;<br>
    ……...<br>
&lt;TaskConfiguration&gt;
</font></td></tr></table>
</li>
<li>Enable API publishing in both the worker nodes by configuring the <I>&lt;IOTS_HOME&gt;/ conf/etc/webapp-publisher-config.xml</I> file as shown below:<br>
<strong>Why is this needed?</strong><br>
This is to publish the synapse configs to the <I>&lt;IOTS_HOME&gt;/repository/deployment/server/synapse-configs/default/api</I> directory. The configs are published when the server starts up. Therefore, disable this setting after the server finishes starting up.
<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
&lt;PublishAPI&gt;true&lt;/PublishAPI&gt;<br>
&lt;EnabledUpdateApi&gt;true&lt;/EnabledUpdateApi&gt;<br>
</font></td></tr></table>
</li>
<li>Start the core profile of the WSO2 IoT Server.

<table border="1" bgcolor=" #D9DEE1">
<tr><td><font style="Arial" size="2">
cd &lt;IOTS_HOME&gt;/bin<br>
./iot-server.sh
</font></td></tr></table>
Now you are done clustering WSO2 IoT Server. You can now start enrolling Android, Windows and IoT devices with WSO2 IoT Server and manage them.

Use the Gateway node (gateway.iots310.wso2.com) to start enrolling Android devices.

Let's take a look at how to cluster WSO2 IoT Server to enroll iOS devices. See, Clustering the iOS server.


</li>