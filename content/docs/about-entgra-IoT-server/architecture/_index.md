---
bookCollapseSection: true
weight: 1
---

# Architecture


[Entgra IoT Server](https://entgra.io/emm) offers an extensible EMM
solution for:
<ul style="list-style-type:disc;">
  <li>Managing mobile devices and laptops of Android, Apple and Windows.</li>
  <li>A scalable architecture to handle virtually any number of devices.</li>
  <li>Managing enterprise and public apps of an organization.</li>
</ul>

Entgra IoT server is built on top of WSO2 Carbon framework and relies on
industry proven API management, identity and access management and
analytics products of WSO2. Each device type such as Android or iOS are
plugable components of the server that can be added or removed making
it easier to add any new device type.

## Entgra IoT Server Architecture

An Entgra IoT Server deployment comprises of the following high-level components:

<img src = "../../image/architecture.png" style="border:5px solid black ">

The following table explains how each component in the Entgra IoT Server
architecture functions as depicted in the figure above:

<table border="1">
    <colgroup>
        <col style="width: 107.0px;">
            <col style="width: 1255.0px;">
    </colgroup>
    <tbody>
        <tr>
            <th>Devices</th>
            <td>
                <div class="content-wrapper">
                    <p>Devices that are getting enrolled to the IoT server will connect over HTTPs to the server to fetch commands. Devices can be iOS, Android, Windows laptops and/or macOS laptops.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>API & Authorization</th>
            <td>
                <div class="content-wrapper">
                    <p>Devices as well as management portals communicate with the server through these APIs and the API management layer adds security, scalability and throttling to APIs. The API management layer of the product is powered by WSO2 API manager. All the product APIs are OAuth protected and REST compliant. All calls will go through an API gateway which communicates with a key validation component to validate OAuth tokens and server requests.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>Device Management Core</th>
            <td>
                <div class="content-wrapper">
                    <p>All generic tasks related to devices regardless of the device type is done by the device management core of the product. This can be simply thought of as the component that does adding, deleting, updating, retrieving device details which are command tasks among all devices. There are many other generic tasks handled by the core such as operation management, policy management, configuration management, license management, communication with push notification servers, user management, etc.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>Device Management Plug-ins</th>
            <td>
                <div class="content-wrapper">
                    <p>Due to the pluggable architecture of the product, each of the mobile/laptop plug-ins that are plugged to the device management core are plug-ins. This allows the server to serve a set of specific plug-ins only. All the device types such as Android, iOS, Windows are plug-ins that get plugged into the core to form the EMM server. Similarly, any IoT plug-in can also be plugged in to the product. While the device management core handles the generic tasks related to devices, each plug-in handles tasks that are specific to each device type. For example, the iOS device management protocol requires XML based messaging format to be used for communication while Android uses JSON. The translation of a generically stored command to the platform-specific format is done inside the relevant plug-in. The plug-in also holds all the APIs that are needed to communicate with device and the APIs that the management UIs need to send commands to specific devices.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>Databases</th>
            <td>
                <div class="content-wrapper">
                    <p>Each plug-in will have its own tables/database and the device management core will also have its own database. In addition to these databases, API management, user management, app management, registry components will have their own databases.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>External Systems</th>
            <td>
                <div class="content-wrapper">
                    <p>In order to do the management tasks related to devices, the server may talk to external systems such as DEP servers from Apple and Google enterprise servers via APIs. The server may also communicate with Cloud messaging services such as FCM and APNS to send wake up messages to devices.</p>
                </div>
            </td>
        </tr>
        <tr>
            <th>Portals</th>
            <td>
                <div class="content-wrapper">
                    <p><strong>Device Management Portal:</strong> Provides the capability for an EMM administrator to manage the device fleet by enrolling and monitoring devices, enforcing policies and commands on the devices. This portal also allows the administrator to manage users, roles, device groups and system configurations.</p>
                    <p><strong>App Publisher Portal:</strong> This portal allows an administrator to publish the enterprise apps that the users are allowed to install on their devices. These include Application Life Cycle Management, where the app developers can publish new versions of the apps and the managers or testers can approve/reject the release and publish them.</p>
                    <p><strong>App Store Portal:</strong> Administrators or users can browse the corporate apps and install them on the devices using this portal. Apps can be bulk installed/updated/deleted from user roles, device groups, individual users or devices. This portal can also be used to schedule application installations to any required off-peak time.</p>
                </div>
            </td>
        </tr>
    </tbody>
</table>

To know more about the Entgra IoT Server architecture, check out the article on <a target="_blank" href="http://wso2.com/library/articles/2017/07/an-introduction-to-wso2-iot-architecture/">An Introduction to Entgra IoT Architecture</a>. 
